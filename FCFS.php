<?php 
class FCFS{

	public $process_events = [];
	public $burst_time     = [];
	public $total_events   = '';
	public $num_b_time   = '';

	//Reciev data and process it
	public function setData(array $input){
		if(array_key_exists('process_events', $input)){
			$this->process_events = explode(',',$input['process_events']);
		}		
		if(array_key_exists('burst_time', $input)){
			$this->burst_time = explode(',',$input['burst_time']);
		}

		$this->total_events = count($this->process_events);
		$this->num_b_time = count($this->burst_time);
		return $this;

	}
	//Function for FCFS algorithm
	public function fcfsAlgorithm(){


		if($this->total_events == $this->num_b_time){

						$limit = $this->total_events;
			$limit = $limit-1;
			$w_time = 0;
			$temp_w_time = 0;

			for ($i= 0; $i < $limit ; $i++) { 

				$w_time = $w_time + $this->burst_time[$i];
				$temp_w_time = $temp_w_time + $w_time;


			}

			$avg_w_time = $temp_w_time / $this->total_events;

			return $avg_w_time;
		}else{
			return false;
		}

	}
	//return Avareg waiting time
	public function getOutput(){

		return $this->fcfsAlgorithm();
	}
}